﻿using System;
using MvvmCross.Core.ViewModels;
using NineGag2.Core.Models;

namespace NineGag2.Core.ViewModels
{
    public class PhotoDetailsViewModel : BaseViewModel<PhotoId>
    {
        private string _newComment;

        public PhotoDetailsViewModel()
        {
            Comments = new MvxObservableCollection<Comment>();
        }

        public string NewComment
        {
            get => _newComment;
            set => SetProperty(ref _newComment, value);
        }

        public IMvxCommand SendCommentCommand => new MvxCommand(() =>
        {
            var comment = new Comment
            {
                Id = 0,
                Content = NewComment,
                Rating = 0,
                PostDate = DateTimeOffset.Now,
                Author = new User
                {
                    Id = 0,
                    AvatarUrl = "http://i.iplsc.com/zdj-ilustracyjne/0006DVXSTLL51M5U-C122-F4.jpg",
                    Name = "Artur",
                    Surname = "Malendowicz",
                    Nickname = "Immons"
                }
            };
            //CommentsService.SendComment(NewComment);
            NewComment = string.Empty;
            Comments.Add(comment);
        });

        public MvxObservableCollection<Comment> Comments { get; }

        public override void Prepare(PhotoId parameter)
        {
            //PhotoDetailsService.GetData(parameter);
        }
    }
}