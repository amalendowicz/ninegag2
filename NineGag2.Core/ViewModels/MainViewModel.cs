﻿using MvvmCross.Core.ViewModels;
using NineGag2.Core.Models;

namespace NineGag2.Core.ViewModels
{
    public class MainViewModel : BaseViewModel<string>
    {
        public MainViewModel()
        {
            Photos = new MvxObservableCollection<Photo>
            {
                new Photo
                {
                    Url = "https://i.ytimg.com/vi/6pVKA31Ks_U/maxresdefault.jpg",
                    Likes = 20,
                    Dislikes = 10
                },
                new Photo
                {
                    Url = "http://i.iplsc.com/zdj-ilustracyjne/0006DVXSTLL51M5U-C122-F4.jpg",
                    Likes = 100,
                    Dislikes = 5
                }
            };
        }

        public MvxObservableCollection<Photo> Photos { get; }

        public IMvxCommand PhotoSelectedCommand => new MvxCommand<Photo>(photo =>
        {
            this.Navigate<PhotoDetailsViewModel, PhotoId>(new PhotoId { Id = photo.Id });
        });

        public override void Prepare(string parameter)
        {
        }
    }
}