﻿using MvvmCross.Core.ViewModels;

namespace NineGag2.Core.ViewModels
{
    public abstract class BaseViewModel<TParameter> : MvxViewModel<TParameter>, INavigatingObject
    {
        public bool Navigate<TViewModel, TParameter>(TParameter parameter,
            IMvxBundle presentationBundle = null)
            where TViewModel : IMvxViewModel<TParameter> where TParameter : class
        {
            return ShowViewModel<TViewModel, TParameter>(parameter, presentationBundle);
        }
    }
}