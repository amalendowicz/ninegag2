﻿using MvvmCross.Core.ViewModels;

namespace NineGag2.Core.ViewModels
{
    public interface INavigatingObject
    {
        bool Navigate<TViewModel, TParameter>(TParameter parameter,
            IMvxBundle presentationBundle = null)
            where TViewModel : IMvxViewModel<TParameter> where TParameter : class;
    }
}