﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;
using NineGag2.Core.Models;

namespace NineGag2.Core.Converters
{
    public class CommentToHeaderStringConverter : MvxValueConverter<Comment, string>
    {
        protected override string Convert(Comment value, Type targetType, object parameter, CultureInfo culture)
        {
            return $"Author: {value.Author.Nickname}; Posted on: {value.PostDate.LocalDateTime.ToLocalTime()}";
        }
    }
}