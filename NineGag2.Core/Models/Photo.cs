﻿using MvvmCross.Core.ViewModels;
using NineGag2.Core.Converters;

namespace NineGag2.Core.Models
{
    public class Photo : BindanbleObject
    {
        private int _likes;
        private int _dislikes;
        public string Url { get; set; }

        public int Likes
        {
            get => _likes;
            set
            {
                if (value == _likes) return;
                _likes = value;
                OnPropertyChanged();
            }
        }

        public int Dislikes
        {
            get => _dislikes;
            set
            {
                if (value == _dislikes) return;
                _dislikes = value;
                OnPropertyChanged();
            }
        }

        public IMvxCommand LikeCommand => new MvxCommand(() => Likes++);

        public IMvxCommand DisikeCommand => new MvxCommand(() => Dislikes++);
        public int Id { get; set; }
    }
}