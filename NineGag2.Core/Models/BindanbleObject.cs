﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using NineGag2.Core.Annotations;

namespace NineGag2.Core.Models
{
    public abstract class BindanbleObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}