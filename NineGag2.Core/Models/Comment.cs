﻿using System;
using NineGag2.Core.ViewModels;

namespace NineGag2.Core.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public User Author { get; set; }

        public int Rating { get; set; }

        public string Content { get; set; }

        public DateTimeOffset PostDate { get; set; }
    }
}