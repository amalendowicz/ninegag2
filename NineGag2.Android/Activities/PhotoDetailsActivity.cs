using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using NineGag2.Core.ViewModels;

namespace NineGag2.Android.Activities
{
    [Activity(Label = "Comments")]
    public class PhotoDetailsActivity : MvxActivity<PhotoDetailsViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_photo_details);
        }
    }
}