using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using NineGag2.Core.ViewModels;

namespace NineGag2.Android.Activities
{
    [Activity(Label = "Photos")]
    public class MainActivity : MvxActivity<MainViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_main);
        }
    }
}